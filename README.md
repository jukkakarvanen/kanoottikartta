# Kanoottikartta

# YLEISTÄ
- Kanoottikartta näyttää maanmittauslaitoksen kartan ja sen päällä kanoottikohteet.
- Kanoottikohteet on otettu sivulta http://www.joensuunkauhojat.net/mobihiisi/kaikki.csv
- Ilmainen apps jossa MIT lisenssi (sillä saa tehdä mitä huvittaa).

KÄYTTÖOHJEET
- Jos jotain menee pieleen: käynnistä apps uudelleen.
- Alkuvalikko:
valitse kartan katselu tai gpx-tiedosto(gpx-reitti ladataan kartan päälle punaisena)
- Meloessa:
pitkä painallus muuttaa kartta-moodia (keskitys+pyöritys) ja (kartan skaalaus/siirtely)
- gpx-reitit on talletettu androidin downloads kansioon ja tiedoston nimen tulee loppua 'gpx'
Käytännössä gmailaa gpx jälki itsellesi mailin liitteenä ja lue maili kännyllä 
- gpx reittejä voi tehdä esim. trailmap.fi tai pullautuskartta.fi

REPO (jossa lähdekoodi jota voit muokata vapaasti):
https://gitlab.com/paapu88/kanoottikartta

ongelmat+parannukset:
markus.kaukonen@student.hanken.fi
