import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:latlong/latlong.dart';
import 'package:proj4dart/proj4dart.dart' as proj4;
import 'package:geolocator/geolocator.dart';
import 'dart:async';

import '../classes/controls.dart';

class CustomCrsPage extends StatefulWidget {
  static const String route = 'custom_crs';

  @override
  _CustomCrsPageState createState() => _CustomCrsPageState();
}

class _CustomCrsPageState extends State<CustomCrsPage> {
  Geolocator geolocator = Geolocator();
  Position userLocation;
  bool found = false;
  double latitude = 60.192059;
  double longitude = 24.945831;
  Proj4Crs epsg3067CRS;
  double maxZoom;
  Future<Rastit> futureRastit;
  proj4.Projection epsg3067;
  // Define start center
  //proj4.Point point = proj4.Point(x: 60.192059, y: 24.945831);
  String initText = 'Map centered to';


  // Mapant data is from http://www.mapant.fi/examples/leaflet/

  @override
  void initState() {
    super.initState();
    futureRastit = fetchRastit();
    // MAYBE EPSG:4326 is a predefined projection ships with proj4dart
    //epsg4326 = proj4.Projection('EPSG:4326');

    // EPSG:3413 is a user-defined projection from a valid Proj4 definition string
    // From: http://epsg.io/3067, proj definition: http://epsg.io/3067.proj4
    // Find Projection by name or define it if not exists
    epsg3067 = proj4.Projection('EPSG:3067') ??
        proj4.Projection.add('EPSG:3067',
            '+proj=utm +zone=35 +ellps=GRS80 +units=m +towgs84=0,0,0,-0,-0,-0,0 +no_defs');
    // mapAnt example zoom level resolutions
    final resolutions = <double>[8192, 4096, 2048, 1024, 512, 256, 128, 64, 32, 16, 8, 4, 2, 1, 0.5, 0.25];

    // [-548576.0, 6291456.0, 1548576.0, 8388608],
    final epsg3067Bounds = Bounds<double>(
      CustomPoint<double>(-548576.0, 6291456.0),
      CustomPoint<double>(1548576.0, 8388608.000000),

    );

    maxZoom = (resolutions.length - 1).toDouble();

    // Define CRS
    epsg3067CRS = Proj4Crs.fromFactory(
      // CRS code
      code: 'EPSG:3067',
      // your proj4 delegate
      proj4Projection: epsg3067,
      // Resolution factors (projection units per pixel, for example meters/pixel)
      // for zoom levels; specify either scales or resolutions, not both
      resolutions: resolutions,
      // Bounds of the CRS, in projected coordinates
      // (if not specified, the layer's which uses this CRS will be infinite)
      bounds: epsg3067Bounds,
      // Tile origin, in projected coordinates, if set, this overrides the transformation option
      // Some goeserver changes origin based on zoom level
      // and some are not at all (use explicit/implicit null or use [CustomPoint(0, 0)])
      // @see https://github.com/kartena/Proj4Leaflet/pull/171
      origins: [CustomPoint(0, 0)],
      // Scale factors (pixels per projection unit, for example pixels/meter) for zoom levels;
      // specify either scales or resolutions, not both
      scales: null,
      // The transformation to use when transforming projected coordinates into pixel coordinates
      transformation: null,
    );
  }

  LatLng get_point() { 
   // function definition 
   return LatLng(latitude, longitude); 
  }

  Future<Position> _getLocation() async {
    var currentLocation;
    try {
      currentLocation = await geolocator.getCurrentPosition(
          desiredAccuracy: LocationAccuracy.best);
    } catch (e) {
      currentLocation = null;
    }
    return currentLocation;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //appBar: AppBar(title: Text('Custom CRS')),
      //drawer: buildDrawer(context, CustomCrsPage.route),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            FutureBuilder<Rastit>(
                future: futureRastit,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text(snapshot.data.track);
                  } else if (snapshot.hasError) {
                    return Text("${snapshot.error}");
                  }

                  // By default, show a loading spinner.
                  return CircularProgressIndicator();
                },
              ),
            Flexible(
              child: FlutterMap(
                options: MapOptions(
                  // Set the default CRS
                  crs: epsg3067CRS,
                  center: get_point(),
                  //center: userCenter,
                  zoom: 8.0,
                  // Set maxZoom usually scales.length - 1 OR resolutions.length - 1
                  // but not greater
                  maxZoom: maxZoom,
                  onTap: (p) => setState(() {
                    _getLocation().then((value) {
                      userLocation = value;
                      latitude = userLocation.latitude;
                      longitude = userLocation.longitude;
                      get_point();
                      //print(latitude);
                    });
                  }),
                ),
                layers: [
                  TileLayerOptions(
                    wmsOptions: WMSTileLayerOptions(
                      // Set the WMS layer's CRS too
                      crs: epsg3067CRS,
                      baseUrl: 'http://wms.mapant.fi/wms.php?',
                      //layers: ['default'],
                      //baseUrl: 'http://avaa.tdata.fi/geoserver/osm_finland/wms?',
                      //layers: ['osm_finland:osm-finland'],
                    ),
                  ),
                  //if (userLocation != null) {
                    CircleLayerOptions(
                        circles: [CircleMarker( //radius marker
                        point: get_point(),
                        color: Colors.blue.withOpacity(0.0),
                        borderStrokeWidth: 3.0,
                        //useRadiusInMeter: true,
                        //radius: 50, //radius
                        //useRadiusInMeter: true,
                        radius: 10, //radius
                        borderColor: Colors.red,
                      )
                    ],
                    ),
                  //}
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
