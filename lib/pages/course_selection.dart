import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong/latlong.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter_file_manager/flutter_file_manager.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import '../widgets/drawer.dart';
import 'package:permission_handler/permission_handler.dart';
import '../pages/map_ant.dart';

class Courses {
  final List<FileSystemEntity> tracks;
  var courses = new List<FileSystemEntity>();

  Courses({this.tracks}) {
    for (final track in tracks) {
      courses.add(track);
    }
  }
  factory Courses.fromJson(List<FileSystemEntity> cs) {
    return Courses(tracks: cs);
  }
}

Future<String> get _localPath async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  final directory = await getExternalStorageDirectory();
  return directory.path;
}

Future<Courses> fetchCourses() async {
  var status = await Permission.storage.status;
  if (!status.isGranted) {
    await Permission.storage.request();
  }
  //final path = await _localPath;
  //print(path);
  //var root = await getExternalStorageDirectory();
  var root = new Directory('/sdcard/download/');
  var courses = await FileManager(root: root).walk().toList();
  print(courses);
  var courses_ok = new List<FileSystemEntity>();
  for (final course in courses) {
    if (course.path.endsWith('gpx')) {
      courses_ok.add(course);
    }
  }
  // sort alphabetically, date only available for File
  courses_ok
      .sort((a, b) => a.path.toLowerCase().compareTo(b.path.toLowerCase()));
  return Courses.fromJson(courses_ok);
}

class CourseSelection extends StatelessWidget {
  static const String route = 'CourseSelection';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Valitse melonta-gpx reitti!"),
      ),
      body: Padding(
        padding: EdgeInsets.all(8.0),
        child: FutureBuilder<Courses>(
          future: fetchCourses(),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Column(
                children: <Widget>[
                  Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.courses.length,
                      itemBuilder: (BuildContext context, int index) {
                        var post = snapshot.data.courses[index];

                        return Container(
                            child: Card(
                          child: ListTile(
                            title: Text(post.path.split("/").last),
                            onTap: () => onTapped(post, context),
                          ),
                        ));
                      },
                    ),
                  ),
                ],
              );
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  void onTapped(FileSystemEntity post, BuildContext context) {
    // navigate to the next screen.
    Navigator.pushReplacementNamed(context, CustomCrsPage.route,
        arguments: post);
  }
}
