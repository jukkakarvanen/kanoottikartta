import 'package:flutter/material.dart';
import '../pages/map_ant.dart';
import '../pages/course_selection.dart';

import '../widgets/drawer.dart';

class HomePage extends StatefulWidget {
  static const String route = '/';

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final titles = ['Näytä Kanoottikartta', 'Valitse melontareitti'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Kanoottikartta"),
      ),
      body: ListView.builder(
        itemCount: titles.length,
        itemBuilder: (BuildContext context, int index) {
          var post = titles[index];

          return Container(
              child: Card(
            child: ListTile(
              title: Text(post),
              onTap: () => onTapped(post, context),
            ),
          ));
        },
      ),
    );
  }

  void onTapped(String post, BuildContext context) {
    // navigate to the next screen.
    if (post == titles[0]) {
      Navigator.pushReplacementNamed(context, CustomCrsPage.route);
    } else {
      Navigator.pushReplacementNamed(context, CourseSelection.route);
    }
  }
}
