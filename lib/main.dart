import 'package:flutter/material.dart';
import './pages/map_ant.dart';
import './pages/course_selection.dart';
import './pages/home.dart';
import 'package:flutter/services.dart';
import 'package:wakelock/wakelock.dart';
import 'package:flex_color_scheme/flex_color_scheme.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Wakelock.enable();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        theme: FlexColorScheme.dark(scheme: FlexScheme.amber, visualDensity: FlexColorScheme.comfortablePlatformDensity).toTheme,
        initialRoute: HomePage.route,
        routes: <String, WidgetBuilder>{
          HomePage.route: (context) => HomePage(),
          //SignInDemo.route: (context) => SignInDemo(),
          CustomCrsPage.route: (context) => CustomCrsPage(),
          CourseSelection.route: (context) => CourseSelection(),
        });
    // home: CustomCrsPage(),
    //routes: <String, WidgetBuilder>{
    //  CustomCrsPage.route: (context) => CustomCrsPage(),
    //  CourseSelection.route: (context) => CourseSelection(),
    //}
  }
}
