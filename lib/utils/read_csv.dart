loadAsset() async {
  var myData = await rootBundle.loadString("assets/kaikki.csv");
  List<List<dynamic>> csvTable = CsvToListConverter().convert(myData);
  setState(() {
    data = csvTable;
  });
}
  